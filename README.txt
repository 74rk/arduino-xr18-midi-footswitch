As Installed Upon Delivery

Author: Rene Knuvers
Date: 2020-10-13
License GPLv3

Runs on an Arduino Nano V3. Tested on a micro too, but requires a few
mods for the on board LEDs.

This software is as it is installed on delivery of the unnit. The
following functions are implemented:
- 2 switches (A and B), to be connected to pin 2 (A) and 3 (B) against
ground on a momentary normally open contact
- 2 bi-color LED's (red-green two pin antiparallel LEDs)
Starts up with a light dimming and color changing sequence and then
sends mute on to all relevant channels

Switch A cycles from both muted to 9 unmuted to 10 unmuted back to both
muted.
Switch B cycles between 11 and 12 muted, or 11 or 12 unmuted if 9 or 10
is unmuted.

Set up the XR18 to have channel 9 and 11 to share the same analog input,
and the same to input 10 and 12. Set channel 9 (10) for the clean / dry
signal and channel 11 (12) for the effect (for example chorus or reverb
or delay). Make sure 11 (12) has LR in the final section off. You want
the clean / dry signal to come from 9 (10) and only the wet signal /
effect from 11 (12). Use the send in 11 (12) to apply the effect.
usually, and depending on the gain settings and the fx return settings,
a send level of -6dB would be okay.
