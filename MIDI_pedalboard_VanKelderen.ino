#include <MIDI.h>

#define MUTE_CHAN_CC 0
#define MUTE_CHAN_CH 2
#define ROB_STAAL 9
#define ROB_NYLON 10
#define ROB_STAAL_CHORUS 11
#define ROB_NYLON_CHORUS 12
#define UNMUTE 0
#define MUTE 127
#define SWITCH_A 2
#define SWITCH_B 3
#define LED_A1 5
#define LED_A2 9
#define LED_B1 6
#define LED_B2 10

bool steelActive = false;
bool nylonActive = false;
bool chorusActive = false;

MIDI_CREATE_DEFAULT_INSTANCE();


void send_mutes() {
  /*
   * Sends midi control changes to mute the channels
   * Uses global variables steelActive and chorusActive
   * Uses definitions for MUTE_CHAN_CC (offset for contron change numbers per channel mute)
   *                      MUTE_CHAN_CH (midi channel for MUTE CHANNEL CC's, 1-16 values!)
   *                      ROB_STAAL, ROB_NYLON, ROB_STAAL_CHORUS and ROB_NYLON_CHORUS channel numbers
   */
  
  if (steelActive) {
    MIDI.sendControlChange(MUTE_CHAN_CC + ROB_STAAL,0,MUTE_CHAN_CH);     // unmute steel
  } else {
    MIDI.sendControlChange(MUTE_CHAN_CC + ROB_STAAL,127,MUTE_CHAN_CH);     // mute steel
  }
  if (nylonActive) {
    MIDI.sendControlChange(MUTE_CHAN_CC + ROB_NYLON,0,MUTE_CHAN_CH);     // unmute nylon
  } else {
    MIDI.sendControlChange(MUTE_CHAN_CC + ROB_NYLON,127,MUTE_CHAN_CH);     // mute nylon
  }

  if (chorusActive) {
    if (steelActive) {
      MIDI.sendControlChange(MUTE_CHAN_CC + ROB_STAAL_CHORUS,0,MUTE_CHAN_CH);     // unmute steel chorus
    } else {
      MIDI.sendControlChange(MUTE_CHAN_CC + ROB_STAAL_CHORUS,127,MUTE_CHAN_CH);     // mute steel chorus
    }
    if (nylonActive) {
      MIDI.sendControlChange(MUTE_CHAN_CC + ROB_NYLON_CHORUS,0,MUTE_CHAN_CH);     // unmute nylon
    } else {
      MIDI.sendControlChange(MUTE_CHAN_CC + ROB_NYLON_CHORUS,127,MUTE_CHAN_CH);     // mute nylon chorus
    }    
  } else {
    MIDI.sendControlChange(MUTE_CHAN_CC + ROB_STAAL_CHORUS,127,MUTE_CHAN_CH);     // mute steel chorus
    MIDI.sendControlChange(MUTE_CHAN_CC + ROB_NYLON_CHORUS,127,MUTE_CHAN_CH);     // mute nylon chorus    
  }
}


void set_LEDS () {
  if (steelActive) {
    digitalWrite(LED_A1, HIGH);
    digitalWrite(LED_A2, LOW);
  } else if (nylonActive) {
    digitalWrite(LED_A1, LOW);
    digitalWrite(LED_A2, HIGH);
  } else {
    digitalWrite(LED_A1, LOW);
    digitalWrite(LED_A2, LOW);    
  }
  if (chorusActive) {
    digitalWrite(LED_B1, HIGH);
    digitalWrite(LED_B2, LOW);
  } else {
    digitalWrite(LED_B1, LOW);
    digitalWrite(LED_B2, LOW); 
  }  
}


void setup() {
  // initialize both on-board leds to be off
//  ledTx(false); // is used to briefly indicate we are transmitting midi
//  ledRx(false);

  MIDI.begin(MIDI_CHANNEL_OFF);                      // Launch MIDI but don't listen (no midi in available)

  // digital IO except for midi (midi out defaults to TX, D1)
  pinMode(SWITCH_A, INPUT_PULLUP);
  pinMode(SWITCH_B, INPUT_PULLUP);
  pinMode(LED_A1, OUTPUT);
  pinMode(LED_A2, OUTPUT);
  pinMode(LED_B1, OUTPUT);
  pinMode(LED_B2, OUTPUT);
  
  // eye candy: Show them I'm alive!
  //  set different colors for each LED
  digitalWrite(LED_A2, LOW);
  digitalWrite(LED_B2, HIGH);
  
  //  fade both in
  for (float i=2; i<255; i=i*1.1) {
    analogWrite(LED_A1, int(i));
    analogWrite(LED_B1, 255 - int(i));
    delay(20);
  }

  //  swap the colors
  digitalWrite(LED_A2, HIGH);
  digitalWrite(LED_B2, LOW);

  //  fade both out
  for (float i=2; i<255; i=i*1.1) {
    analogWrite(LED_A1, int(i));
    analogWrite(LED_B1, 255 - int(i));
    delay(20);
  }

  // send initial state of the mutes
  send_mutes();
  // set LEDs to reflect initial state
  set_LEDS();
  
} // end void setup()

void loop() {
  bool buttonsChanged = false;
  bool statusSwitchA = digitalRead(SWITCH_A);
  bool statusSwitchB = digitalRead(SWITCH_B);
  
  if (!(statusSwitchA) && steelActive) {
    steelActive = false;
    nylonActive = true;
    buttonsChanged = true;
  } else if (!(statusSwitchA) && nylonActive) {
    steelActive = false;
    nylonActive = false;
    buttonsChanged = true;
  } else if (!(statusSwitchA) && !(steelActive) && !(nylonActive)) {
    steelActive = true;
    nylonActive = false;
    buttonsChanged = true;
  } else if (!(statusSwitchB) && chorusActive) {
    chorusActive = false;
    buttonsChanged = true;
  } else if (!(statusSwitchB) && !(chorusActive)) {
    chorusActive = true;
    buttonsChanged = true;
  }
  
  if (buttonsChanged) {
    send_mutes();
    set_LEDS();   // now it the time to update the two-color LED's above the switches to indicate the state I'm in...

    // Lets wait untill both switches are back into the resting position.
    // This is for debouncing, but also we don't want switches to auto-repeat
    int i=255;
    while (statusSwitchA) {
      delay(1);
      if (digitalRead(SWITCH_A)) {
        i--;
        if (i == 0) {
          statusSwitchA = false;
        }
      } else {
        int i=255;
      }
    }
    while (statusSwitchB) {
      delay(1);
      if (digitalRead(SWITCH_B)) {
        i--;
        if (i == 0) {
          statusSwitchB = false;
        }
      } else {
        int i=255;
      }
    }
  } // end buttons chamged loop
} // end void loop()
